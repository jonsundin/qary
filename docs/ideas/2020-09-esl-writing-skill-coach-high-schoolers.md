
# ESL Teaching Games from Study.com


## Listen, Draw, Tell

Students draw while they listen. They should be guided to draw based on what they hear.

### Mystery Object Drawing Contest

*AI:* This game can be automated and graded by AI.

For this activity, you will be looking at the picture of a 'mystery object' that your students will try to guess by drawing it based on your instructions. Basically, you will provide students with simple directions so they can draw a specific item without looking at the picture and without you telling them what the object is.

For instance, say you are looking at the picture of a house. Your directions could include some of the following:

- Draw a large square
- Above the square, draw a triangle
- At the bottom of the square, draw a rectangle
- On the upper right corner of that rectangle, draw a small circle

Students will have different interpretations of what they are supposed to draw.They will have activated their listening skills while trying to decipher what the final object should look like based on what they heard.

This activity can be adapted to students of different ages and linguistic levels by increasing the complexity of the drawing instructions. To make it more of a game, you could have a class vote to determine whose drawing most closely resembles the item you described.

### Draw a Story

Have students 'draw' a story they hear. As you tell a short story, have students draw pictures representing that story. Then, you can have students re-tell the story based on their drawings. The entire class will be entertained as they hear all kinds of versions of the same story.

### Follow Verbal Directions

*AI:* Not automatable unless it involves directions that can be followed online or using apps on their phone/pc.

Students of all ages can be asked to follow a set of spoken directions.

For example, you could give your students a piece of paper each and have them follow your directions to make a paper airplane:

1. First, fold the bottom edge of the paper up to meet the top edge.
2. Then, unfold the paper and take the top left corner, and fold it over to meet the bottom edge, which will form a small right triangle.
3. Now, fold down the top right corner so that the two triangles are aligned.
4. ...

Once your students are done, you all can see how well they followed directions by flying the airplanes and seeing how far and fast they fly.

### Guess the Object

*AI:* Automatable? Social app?

Turning a listening activity into a friendly competition is an easy way to keep students engaged. Model this Guess the Object activity by describing an object found in the classroom in such detail that students can easily guess what it is.

Then, have students take turns choosing an object and describing it to the class without stating the object's name.

You can keep a running tally and give points to the first student who correctly guesses the object each time -- or you could set a timer so the student describing the object has do so quickly yet accurately.
