# Sprint 4 Retro & Plan

2020-08-27

## Sprint 4 plan
1. Log for speed an accuracy every time we push to Master
    - should record what the CPU and RAM details
    - Stretch goal: should record what the CPU and RAM details are
2. Ability to upload a text document qary index "file path or URL", and will incorporate it into text_qa bot
3. Create the infrastructure On the conversation_planner level
    - add an action e.g. "qary play music"
    - command line option -a playmusic
4. Spelling corrector feature to the existing qa_bot pipeline
    - Stretch goal: incorporating it into the spacy model.

## Hobson:
- Help me filter the information flow & news sources
- Ability to filter "Slightly misinformational" news
- Summarization

## Leo:
- Semantic search on the question answering bot (stretch: do it on your personal computer personal computer
- Ability to launch background service / daemon

## Maria:
- Learn from text
- Reliability of the question answering

## Duncan:
- actionable suggestions from self-help parenting book

## Mohammed:
- faster?

